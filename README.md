# STECmetadetector

Snakemake workflow for detection and characterization of multiple Ecoli strains from longread metagenomic sample experiments

## Overview

The workflow performs

* Read filtering and trimming
* Read chracterizatoin for identification of multiple alleles of O-antigens, stx and eae
* Long-read assembly using flye
* Assembly quality control using checkm
* Assembly characterization using mlst, serotyping and pathotyping
* Haplotype search using strainberry and subsequent characterization of each detected strain


## Authors

* Sandra Jaudou (@Sjaudou)
* Carlus Deneke (@deneke)


## Rule graph 

![](dag.png)

## Setup

1. Clone this repository

```
cd path/2 # choose location of repository
git clone --recurse-submodules https://gitlab.com/bfr_bioinformatics/STECmetadetector
```

The workflow will be path/2/STECmetadetector

2. All dependencies can be installed using conda/mamba

### Single environemt

A single environment containing __all__ dependencies can be created using

```
mamba env create -n metadetect -f path/2/metadetect/envs/metadetect.yaml
```


### Module specific environemts

Alternatively, snakemake can orchestrate the installation of all dependencies for each module. The requirement is a snakemake environment, e.g.

```
mamba create -n snakemake snakemake-minimal pandas mamba
```

All dependencies are installed upon first execution or by 

```
cd path/2/repo # adapt location of repository
snakemake --snakefile Snakefile --configfile config.yaml --use-conda --conda-prefix conda_env --conda-create-envs-only -j 1
```


Note that mamba is preferred over conda as it is better able to solve dependencies. If mamba is not available replacte mamba by conda and specify `--conda-frontend conda` in the snakemake command.

3. Install databases

Most databases are provided by the repository and the conda packages. 

Additionally, a kraken2 database (for instance minikraken2 database) is required. The kraken2's main objective is to classify reads as E. coli. You can use an existing database at a different location and adapting the workflow configuration or copy or link a database into `path/2/repo/databases/kraken2`.

For example you can obtain the database as follows

```
cd path/2/repo
wget -P databases https://genome-idx.s3.amazonaws.com/kraken/k2_standard_8gb_20210517.tar.gz
mkdir -p databases/kraken2 & tar -xvzf databases/k2_standard_8gb_20210517.tar.gz --directory databases/kraken2
```

### Installation using docker

A docker container is also available on [docker hub](https://hub.docker.com/r/bfrbioinformatics/stecmetadetector). For details, see the [docker documentation](https://gitlab.com/bfr_bioinformatics/STECmetadetector/-/blob/docker/docker/README_docker.md).

The docker container does not include the kraken2 database.

## Execution

### Creation of sample sheet

All information of the input data is collected in a sample sheet. It must be a tsv with the columns _sample_ and _reads_, where sample is the sample name and reads the full path to the ONT reads.

A sample sheet can be creating by e.g.

```
cd path/2/reads

echo -e "sample\treads" > samples.tsv
for file in `pwd`/*.fastq.gz; do name=`basename -s .fastq.gz $file`; echo -e "$name\t$file" >> samples.tsv; done

```

The resulting file _samples.tsv_ is the input to the workflow.

If the read data was provided by more than one file, please concatenate the reads before.

### Using the wrapper

You can run the workflow using the wrapper function `STECmetadetector.py`:

```
python STECmetadetector.py -l path/2/samples.tsv -d path/2/results
```

* Specify the path to sample sheet after `-l`
* Specify your desired result directory after `-d`
* before exution add `-n` in order to perform a dryrun
* for all options, see python STECmetadetector.py --help



### Using the snakemake command

Alternatively you can run it also directly using the snakemake command

```
snakemake --snakefile Snakefile --configfile config.yaml --cores 10
```

* before execution add `-n` in order to perform a dryrun
* specify `--use-conda --conda-prefix conda_env` if desired (see above)
* when running on a cluster, use `--cluster`
* for all available options in snakemake see `snakemake --help`

You can either create a config.yaml using the wrapper or by editing the file _config.yaml_ provided in this repository.


## Using the strainberry-parsing subworkflow

Once the execution of metadetect has completed, a further characerization of all phases predicted by strainberry is possible.

The rules contained in `strainberry_parsing.smk` perform a dephasing (i.e. collecting all contigs that belong to a predicted strain), followed by serotyping and shigatyping of each detected phase.

```
cd workdir # path where metadetect results were stored (and where config.yaml is located)
snakemake --snakefile path/2/repo/metadetect/rules/strainberry_parsing.smk --configfile config.yaml -f all_checkpoint_strainberry -j 10 
```

You may adapt the number of CPUs by changing the `-j 10` argument.

## Output description

The individual tool's results are organized as `workdir/{tool}/{sample}`, for example `metadetect_results/checkm/sample1/results_checkM_genome.tsv`

* fastq_stats/raw/sample1/NanoStats.txt
* fastq_stats/filtered/sample1/NanoStats.txt
* kraken2/sample1.mpa
* assembly/sample1/assembly.fasta
* assembly/sample1/assembly_info.txt
* checkm/sample1/results_checkM_genome.tsv
* mlst/sample1/report.tsv
* serotyping/sample1/report.tsv
* virulencefactors/sample1/report.tsv
* mapping_characterization/summary_sample1.tsv
* strainberry/sample1/strainberry_n2/assembly.scaffolds.info.tsv

#### summary files (for all samples)

* summary/characterization_summary.tsv  
* summary/kraken2_summary_allgenera.tsv  
* summary/mapping_summary.tsv  
* summary/sample2_summary.tsv
* summary/kraken2.png                   
* summary/kraken2_summary.tsv            
* summary/strainberry_summary.tsv

### strainberry-parsing output


* strainberry/sample1/dephased_contigs
* strainberry/sample1/summary/dephasing_stats.tsv  
* strainberry/sample1/summary/ecolivf_stats.tsv  
* strainberry/sample1/summary/summary_phase.tsv
* strainberry/sample1/summary/ecoh_stats.tsv       
* strainberry/sample1/summary/results_phase.tsv

* summary/strainberry_summary.tsv

#### HTML report
* reports/summary_report.html

Example results can be found in testdata/results.

## Testing

A minimal test data set containing subsampled ONT reads are provided in `testdata`. It contains two samples (sample1.fastq.gz  sample2.fastq.gz) with 1000 reads each. This set is meant to test the pipelines functionality.


The metagenomic data from the publication can be found in BioProject PRJNA835223.

### Using the wrapper

```
./STECmetadetector.py -d testdata/results_test -l testdata/samples.tsv -n
```

* This creates a config file config.yaml with all paths set
* You may need to modify paths to e.g. kraken2 database
* Start execution by omitting the `-n` flag
* Expected results are shown in testdata/results
* Adapt as explained above, e.g. by specifying `--use-conda`

## References

The following tools are used in MetaDetect:

  * snakemake
  * kraken2
  * krakentools
  * nanofilt
  * flye
  * porechop
  * mlst
  * minimap2
  * abricate
  * checkm-genome
  * samtools
  * bedtools
  * nanoplot
  * strainberry
  * R and dplyr, ggplot2, rmarkdown packages
  * bioconductor
