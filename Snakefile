import os
import pandas as pd
import shutil

shell.executable("bash")

#print("I am here:")
#print(os.getcwd())
#print("workdir: " + config["workdir"])
#print("samples: " + config["samples"])

path2strainberry = os.path.join(workflow.basedir,"strainberry")

# Set snakemake main workdir variable
workdir: config["workdir"]

# samples
samples = pd.read_table(config["samples"], index_col="sample")
samples.index = samples.index.astype('str', copy=False) # in case samples are integers, need to convert them to str

rule all:
    input:
        expand("fastq_stats/raw/{sample}", sample=samples.index),
        expand("trimmed/{sample}.fastq.gz", sample=samples.index),
        expand("trimmed_filtered/{sample}.fastq.gz", sample=samples.index),
        expand("fastq_stats/filtered/{sample}", sample=samples.index),
        # classification rules
        expand("kraken2/{sample}.kraken2", sample=samples.index),
        expand("extracted_reads/{sample}.fastq", sample=samples.index),
        expand("kraken2/{sample}.mpa", sample=samples.index),
        # mapping based checks
        "summary/mapping_summary.tsv",
        # "blast_results.tsv if config[["params"]][["do_blast"]] else [], # juist and example
        # expand("stx_eae_O-group_detection/{sample}.blast.tsv", sample=samples.index), # Removed until rule is fixed!
        # assembly
        expand("assembly/{sample}/assembly.fasta", sample=samples.index),
        #expand("{sample}/stats/nanostats_raw.tsv", sample=samples.index)
        # characterization rules
        expand("mlst/{sample}/report.tsv", sample=samples.index),
        expand("serotyping/{sample}/report.tsv", sample=samples.index),
        expand("virulencefactors/{sample}/report.tsv", sample=samples.index),
        expand("checkm/{sample}/results_checkM_genome.tsv", sample=samples.index),
        # plotting and reporting rules
        "summary/kraken2.png",
        # strainberry rules
        expand("strainberry/mapped_reads/{sample}_sorted.bam", sample=samples.index),
        #expand("strainberry/index/{sample}_mapping.bam", sample=samples.index),
        #expand("strainberry/index/{sample}/assembly.fasta", sample=samples.index),
        #expand("strainberry/{sample}/assembly.scaffolds.fa", sample=samples.index), # No longer the target rule!
        expand("strainberry/{sample}/.completed", sample=samples.index),
        "summary/characterization_summary.tsv",
        "reports/summary_report.html"



rule all_strainberry:
    input:
        #expand("strainberry/{sample}/assembly.scaffolds.fa", sample=samples.index)
        #expand("strainberry/{sample}/", sample=samples.index)
        #expand("strainberry/{sample}", sample=samples.index)
        expand("strainberry/{sample}/.completed", sample=samples.index)

rule all_mapping:
    input:
        # TODO add results of your blast rules here
        "summary/mapping_summary.tsv"
        #expand("stx_eae_O-group_detection/{sample}.blast.tsv", sample=samples.index),
        #expand("stx_eae_O-group_detection/summary_{sample}.tsv", sample=samples.index),


rule all_without_strainberry:
    input:
        expand("fastq_stats/raw/{sample}", sample=samples.index),
        expand("trimmed/{sample}.fastq.gz", sample=samples.index),
        expand("trimmed_filtered/{sample}.fastq.gz", sample=samples.index),
        expand("fastq_stats/filtered/{sample}", sample=samples.index),
        expand("kraken2/{sample}.kraken2", sample=samples.index),
        expand("extracted_reads/{sample}.fastq", sample=samples.index),
        expand("kraken2/{sample}.mpa", sample=samples.index),
        expand("assembly/{sample}/assembly.fasta", sample=samples.index),
        "summary/mapping_summary.tsv", 
        #expand("{sample}/stats/nanostats_raw.tsv", sample=samples.index)
        #"finish"
        # characterization rules
        expand("mlst/{sample}/report.tsv", sample=samples.index),
        expand("serotyping/{sample}/report.tsv", sample=samples.index),
        expand("virulencefactors/{sample}/report.tsv", sample=samples.index),
        expand("checkm/{sample}/results_checkM_genome.tsv", sample=samples.index),
        "summary/kraken2.png",
        "summary/characterization_summary.tsv",
        "reports/summary_report.html"

rule all_report:
    input:
        "reports/summary_report.html"


# functions -----------------------------------------
def _get_fastq(wildcards,read_pair='fq1'):
    return samples.loc[(wildcards.sample), [read_pair]].dropna()[0]


# rules -----------------------------------


rule fastq_stats:
    input:
        reads = lambda wildcards: _get_fastq(wildcards, 'reads'),
    output:
        nanoplot_all_reads = directory("fastq_stats/raw/{sample}") # TODO change to a single output file and add this to the all rule
    message: "Nanoplot on all reads for sample {wildcards.sample}"
    conda: "envs/nanoplot.yaml"
    threads: config["parameters"]["threads"]
    log: "logs/nanoplot_raw_{sample}.log"
    params:
        #outdir = "fastq_stats/raw/{sample}" # TODO 
    shell:
        """
        NanoPlot --version > {log}
        echo "NanoPlot --fastq {input.reads} -o {output.nanoplot_all_reads}" >> {log}
        NanoPlot --fastq {input.reads} -o {output.nanoplot_all_reads} 
        """ 
        

rule trim_reads:
    input: 
        reads = lambda wildcards: _get_fastq(wildcards, 'reads'),
    output:
        trimmed_reads = "trimmed/{sample}.fastq.gz"
    message: "Trimming reads for sample {wildcards.sample}"
    conda: "envs/porechop.yaml"
    threads: config["parameters"]["threads"]
    log: "logs/porechop_{sample}.log"
    shell:
        #"touch {output}"
        "porechop -i {input.reads} -o {output.trimmed_reads} --threads {threads} &> {log}"


rule filter_reads:
    input:
        trimmed_reads = "trimmed/{sample}.fastq.gz"
    output:
        filtered_reads = "trimmed_filtered/{sample}.fastq.gz"
    message: "Running nanofilt on {wildcards.sample}"
    conda: "envs/nanofilt.yaml"
    log: "logs/nanofilt_{sample}.log"
    threads:
        config["parameters"]["threads"]
    params:
        minquality = config["parameters"]["filtering"]["minquality"],#7, 
        minlength = config["parameters"]["filtering"]["minlength"],#1000,
        extra = config["parameters"]["filtering"]["extra"],#"" #--maxlength --minGC --maxGC --headcrop --tailcrop --readtype 1D
    shell:
        """
        gunzip -c {input} | NanoFilt --quality {params.minquality} --logfile {log} --length  {params.minlength} {params.extra} | gzip > {output}
        """


rule fastq_stats_filtered:
    input:
        filtered_reads = "trimmed_filtered/{sample}.fastq.gz"
    output:
        nanoplot_filtered_reads = directory("fastq_stats/filtered/{sample}")
    message: "Nanoplot on filtered reads for sample {wildcards.sample}"
    conda: "envs/nanoplot.yaml"
    threads: config["parameters"]["threads"]
    log: "logs/nanoplot_filtered_{sample}.log"
    shell:
        "NanoPlot --fastq {input.filtered_reads} -o {output.nanoplot_filtered_reads} "


rule classify_reads:
    input: 
        filtered_reads = "trimmed_filtered/{sample}.fastq.gz"
    output:
        kraken2 = "kraken2/{sample}.kraken2",
        kraken2report = "kraken2/{sample}.kraken2report"
    message: "Running kraken2 on {wildcards.sample}"
    conda: "envs/kraken2.yaml"
    log: "logs/kraken2_{sample}.log"
    threads:
        config["parameters"]["threads"]
    params:
        kraken2db = config["parameters"]["kraken2db"]
    shell:
        "kraken2 --db {params.kraken2db} --threads {threads} --gzip-compressed --output {output.kraken2} --report {output.kraken2report} {input} &> {log}"


#convert the reports to mpa style and combine them
#or see with carlus for its 10 hit summary file
#then add the R script

rule extract_reads:
    input:
        kraken2 = "kraken2/{sample}.kraken2",
        kraken2report = "kraken2/{sample}.kraken2report",
        filtered_reads = "trimmed_filtered/{sample}.fastq.gz"
    output:
        extracted_reads = "extracted_reads/{sample}.fastq"
    message: "Extraction E. coli reads from {wildcards.sample}"
    conda: "envs/kraken2.yaml"
    log:
        "logs/extracted_reads_{sample}.log"
    params: 
        species = "Escherichia coli",
        taxid = config["parameters"]["taxid"] # 562
    shell:
        "extract_kraken_reads.py -k {input.kraken2} --report {input.kraken2report} -s {input.filtered_reads} -t {params.taxid} --include-children --fastq-output -o {output} &>> {log}"



rule convert_to_mpa:
    input:
        kraken2report = "kraken2/{sample}.kraken2report",
    output:
        mpa_report = "kraken2/{sample}.mpa"
    message: "Converting kraken2report to mpa for {wildcards.sample}"
    conda: "envs/kraken2.yaml" #in krakentools
    log:
        "logs/convert_to_mpa_{sample}.log"
    shell:
        "kreport2mpa.py -r {input.kraken2report} -o {output.mpa_report} &>> {log}"



rule plot_kraken:
    input: 
        mpa_reports = expand("kraken2/{sample}.mpa", sample=samples.index)
    output:
        barplot = "summary/kraken2.png",
        summary_table = "summary/kraken2_summary.tsv",
        summary_table_all = "summary/kraken2_summary_allgenera.tsv"
    message: "Creating kraken2 barplot"
    conda: "envs/rmarkdown.yaml"
    params:
        taxlevel = "genus",
        max_taxa = 12,# TODO add to configfile
        min_abundance = 0.5,
        filter_percentage = True,
    #log: "logs/plot_kraken.log"
    script:
        "scripts/plot_kraken.R"



# -----------
# mapping characterization
# purpose: find multiple O groups, detect stx and eae

rule detect_stx_eae_Ogroup:
    input:
        extracted_reads = "extracted_reads/{sample}.fastq"
    output:
        mapping_out = "mapping_characterization/{sample}_vs_Ogroup_stx_eae.paf"
    message: "Detecting stx eae and O-group for {wildcards.sample}"
    conda: "envs/mapping.yaml"
    threads: config["parameters"]["threads"]
    log:
        "logs/detect_stx_eae_O-group_{sample}.log"
    params: 
        db = config["parameters"]["blastn"]["db"] # TODO add to config
    shell:
        """
        minimap2 --version > {log}
        echo "minimap2 -t {threads} -x map-ont {params.db} {input.extracted_reads} -o {output.mapping_out}" | tee -a {log}
        minimap2 -t {threads} -x map-ont {params.db} {input.extracted_reads} -o {output.mapping_out}  | tee -a {log}
        """

rule parse_mapping:
    input:
        mapping_out = "mapping_characterization/{sample}_vs_Ogroup_stx_eae.paf"
    output:
        summary = "mapping_characterization/summary_{sample}.tsv"
        #summary = "summary/summary_mapping_{sample}.tsv"
    message: "parsing stx eae and O-group for {wildcards.sample}"
    params:
        min_gene_coverage = 0.8,
        min_gene_depth = 1,
        sample = "{sample}" # {wildcards.sample}
    script:
        "scripts/parse_minimap_characterization.R"

rule collect_all_mapping_summaries:
    input:
        summary = expand("mapping_characterization/summary_{sample}.tsv", sample = samples.index)
    output:
        summary = "summary/mapping_summary.tsv"
    message: "collecting all samples mapping summaries"
    run:
        with open(output.summary, 'wb') as outfile:
            for i, fname in enumerate(input.summary):
                with open(fname, 'rb') as infile:
                    if i != 0:
                        infile.readline()  # Throw away header on all but first file
                    shutil.copyfileobj(infile, outfile)      # Block copy rest of file from input to output without parsing
        

# -----------

rule assembly:
    input:
        extracted_reads = "extracted_reads/{sample}.fastq"
    output:
        contigs = "assembly/{sample}/assembly.fasta",
        stats = "assembly/{sample}/assembly_info.txt",
    message: "Running flye on {wildcards.sample}"
    conda: "envs/flye.yaml"
    log:
        "logs/flye_{sample}.log"
    threads:
        config["parameters"]["threads"]
    params:
        #genome_size = ""
        outdir = "assembly/{sample}",
        read_type = "--nano-raw", #config["parameters"]["flye"]["read_type"], #choose from --pacbio-raw   --pacbio-corr   --pacbio-hifi   --nano-raw   --nano-corr   --subassemblies
        iterations = 3, #config["parameters"]["flye"]["iterations"], #1 number of polishing iterations 
        extra = "", #config["parameters"]["flye"]["extra"], #--plasmids" # extra options
    shell:
        """
        flye --version > {log}
        echo "flye {params.read_type} {input.extracted_reads} {params.extra} --threads {threads} --iterations {params.iterations} --out-dir {params.outdir}" >> {log}
        flye {params.read_type} {input.extracted_reads} {params.extra} --threads {threads} --iterations {params.iterations} --out-dir {params.outdir} &>> {log}
        """

#rule run_mlst:
rule mlst_calling:
    input:
        contigs = "assembly/{sample}/assembly.fasta",
    output:
        report = "mlst/{sample}/report.tsv",
        #json  = "results/{sample}/mlst/report.json"
    message: "Running rule mlst_calling on {wildcards.sample}"
    conda: "envs/mlst.yaml"
    log: "logs/mlst_{sample}.log"
    params:
        tmpreport = "mlst/{sample}/report.tsv.tmp",
    shell:
        """
        mlst --scheme ecoli {input} > {params.tmpreport} 2> >(tee {log} >&2)
        paste <(echo {wildcards.sample}) <(cut -f 2- {params.tmpreport}) > {output.report}
        rm {params.tmpreport}
        """

#rule check_mlst:
rule run_abricate_OH:
    input:
        contigs = "assembly/{sample}/assembly.fasta"
    output:
        report = "serotyping/{sample}/report.tsv"
    message: "Running rule run_abricate_OH on {wildcards.sample} with contigs"
    conda: "envs/abricate.yaml"
    log:
      "logs/Genial_OH_{sample}.log"
    threads:
        config["parameters"]["threads"]
    params:
        refdb = config["parameters"]["OH"]["path"],
        dbname = config["parameters"]["OH"]["name"],
        minid = config["parameters"]["OH"]["minid"],
        mincov = config["parameters"]["OH"]["mincov"]
    shell: 
        """
        abricate --version > {log}
        echo "abricate --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report}" >> {log}
        abricate --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2> >(tee {log} >&2)
        """

rule run_abricate_ecoli_vf:
    input:
        contigs = "assembly/{sample}/assembly.fasta",
    output:
        report = "virulencefactors/{sample}/report.tsv"
    message: "Running rule run_abricate_ecoli_vf on {wildcards.sample} with contigs"
    conda: "envs/abricate.yaml"
    log:
        "logs/abricate_ecoli_vf_{sample}.log"
    threads:
        config["parameters"]["threads"]
    params:
        refdb = config["parameters"]["ecovf"]["path"],
        dbname = config["parameters"]["ecovf"]["name"],
        minid = config["parameters"]["ecovf"]["minid"],
        mincov = config["parameters"]["ecovf"]["minid"]
    shell: 
        "abricate --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2> >(tee {log} >&2)"




# TODO
#rule detect_stx:
#rule detect_eae:


#rule checkm 
rule run_checkm_taxonomy:
    input: 
        final_assembly = "assembly/{sample}/assembly.fasta"
    output: 
        checkm_table = "checkm/{sample}/results_checkM_genome.tsv"
    message: "Running checkm_taxonomy on {wildcards.sample}"
    conda: "envs/checkm.yaml"
    log:
        "logs/checkm_taxonomy_{sample}.log"
    params:
        outdir = "checkm/{sample}",
        assemblydir= "assembly/{sample}",
        taxlevel = "species"
    threads:
        config["parameters"]["threads"]
    shell:
        """
        checkm taxonomy_wf -x fasta --tab_table -f {output.checkm_table}  -t {threads} {params.taxlevel} "Escherichia coli" {params.assemblydir} {params.outdir} &> {log}
        """

# strainbverry rules ---------------------


rule mapping_reads_assembly:
# + rule sort_mapping
    input:
        contigs = "assembly/{sample}/assembly.fasta",
        extracted_reads = "extracted_reads/{sample}.fastq"
    output:
        #mapped_reads = "strainberry/mapped_reads/{sample}.sam",
        #bam = "strainberry/mapped_reads/{sample}.bam",
        sorted_bam = "strainberry/mapped_reads/{sample}_sorted.bam"
    message: "Mapping reads on {wildcards.sample} assembly"
    conda: "envs/mapping.yaml"
    log:
        "logs/strainberry_mapping_{sample}.log"
    threads:
        config["parameters"]["threads"]
    shell:
        "minimap2 -t {threads} -ax map-ont {input.contigs} {input.extracted_reads} | samtools view -Sb --threads {threads} | samtools sort --threads {threads} > {output.sorted_bam}"
        # full
        #"minimap2 -ax map-ont {input.contigs} {input.extracted_reads} | samtools view -Sb {output.mapped_reads} | samtools sort {output.bam} > {output.sorted_bam}"
        
        # original
        # "minimap2 -ax map-ont {input.contigs} {input.extracted_reads} > {output.mapped_reads} | samtools view -Sb {output.mapped_reads} > {output.bam} | samtools sort {output.bam} > {output.sorted_bam}"

rule index_mapping:
    input:
        sorted_bam = "strainberry/mapped_reads/{sample}_sorted.bam"
    output:
        #mapping = "strainberry/index/{sample}_mapping.bami"
        indexed_bam = "strainberry/mapped_reads/{sample}_sorted.bam.bai"
    message: "Indexing {wildcards.sample} mapping"
    conda: "envs/mapping.yaml"
    log:
        "logs/strainberry_index_mapping_{sample}.log"
    threads:
        config["parameters"]["threads"]
    shell:
        "samtools index {input.sorted_bam}"

rule index_genome:
    input:
        contigs = "assembly/{sample}/assembly.fasta",
    output:
        #indexed_contigs = "strainberry/index/{sample}/assembly.fasta",
        contigs = "assembly/{sample}/assembly.fasta.fai",
    message: "Indexing {wildcards.sample} assembly"
    conda: "envs/mapping.yaml"
#    log:
#        "logs/strainberry_index_genome_{sample}.log"
    threads:
        config["parameters"]["threads"]
    shell:
        "samtools faidx {input.contigs}"
        #"samtools faidx {input.contigs} > {output.indexed_contigs}"

rule strainberry:
    input:
        #mapping = "strainberry/index/{sample}_mapping.bam",
        indexed_bam = "strainberry/mapped_reads/{sample}_sorted.bam.bai",
        mapping = "strainberry/mapped_reads/{sample}_sorted.bam",
        #indexed_contigs = "strainberry/index/{sample}/assembly.fasta"
        contigs = "assembly/{sample}/assembly.fasta",
        contig_index = "assembly/{sample}/assembly.fasta.fai",
    output:
        #strainberry_directory = directory("strainberry/{sample}"),
        #strainberry_assembly = "strainberry/{sample}/assembly.scaffolds.fa" # not always present
        strainberry_completed = "strainberry/{sample}/.completed"
    message: "Running strainberry on {wildcards.sample}"
    conda: "envs/strainberry.yaml"
    log:
        "logs/strain_separation_{sample}.log"
    threads:
        config["parameters"]["threads"]
    params:
        outdir = "strainberry/{sample}"
    shell:
        """
        {path2strainberry}/strainberry --cpus {threads} -r {input.contigs} -b {input.mapping} -o {params.outdir} && touch {output.strainberry_completed}
        """


# strainberry details are in rules/strainberry_parsing.smk
#include("rules/strainberry_parsing.smk")

# reporting ---------------------

rule sample_summary:
    input:
        #NanoStats_raw_txt = "fastq_stats/raw/{sample}/NanoStats.txt",
        #NanoStats_filtered_txt = "fastq_stats/filtered/{sample}/NanoStats.txt",
        kraken2_mpa = "kraken2/{sample}.mpa",
        assembly_fasta = "assembly/{sample}/assembly.fasta",
        assembly_info_txt = "assembly/{sample}/assembly_info.txt",
        results_checkM_genome_tsv = "checkm/{sample}/results_checkM_genome.tsv",
        #bin_stats_analyze_tsv = "checkm/{sample}/storage/bin_stats.analyze.tsv",
        mlst_report_tsv = "mlst/{sample}/report.tsv",
        ecoh_report_tsv = "serotyping/{sample}/report.tsv",
        vf_report_tsv = "virulencefactors/{sample}/report.tsv",
        #mapping_characterization/summary_{sample}.tsv
        #strainberry/{sample}/strainberry_n2/assembly.scaffolds.info.tsv
        #strainberry_dir = "strainberry/{sample}"
        #summary/kraken2_summary.tsv
        #summary/strainberry_summary.tsv
    output: 
        summary = "sample_summary/characterization_{sample}.tsv"
    message: "Summarizing findings for sample {wildcards.sample}"
    #log:
    conda: "envs/rmarkdown.yaml"
    params:
        #working_dir = config["workdir"]
        sample = "{sample}"
    script:
        "scripts/collect_sample_results.R"
        
# collect for all samples
rule collect_characterization_summaries:
    input:
        summary = expand("sample_summary/characterization_{sample}.tsv", sample = samples.index)
    output: 
        summary = "summary/characterization_summary.tsv"
    message: "collect all samples characterization summaries"
    run:
        with open(output.summary, 'wb') as outfile:
            for i, fname in enumerate(input.summary):
                with open(fname, 'rb') as infile:
                    if i != 0:
                        infile.readline()  # Throw away header on all but first file
                    shutil.copyfileobj(infile, outfile)      # Block copy rest of file from input to output without parsing


rule create_summary_report:
    input:
        characterization_summary = "summary/characterization_summary.tsv",
        mapping_summary = "summary/mapping_summary.tsv",
        barplot = "summary/kraken2.png",
        kraken2_summary = "summary/kraken2_summary.tsv",
        kraken2_summary_all = "summary/kraken2_summary_allgenera.tsv"
    output: "reports/summary_report.html"
    message: "Creating summary report for all samples"
    conda: "envs/rmarkdown.yaml"
    params:
        # strainberry results are optional
        strainberry_summary = "summary/strainberry_summary.tsv",
        strainberry_results = expand("strainberry/{sample}/summary/results_phase.tsv", sample = samples.index)
    script:
        "scripts/summary_report.Rmd"

# TODO define input for summary report
#rule create_summary_report:
    #input:
        #read_stats = expand() # TODO specify
        #blast_stats = expand()
        #assembly_stats = expand()
        #checkm_stats = expand()
        #abricate_stats = expand()
        #strainberry_stats = expand()
        ## TODO more
    #output:
#        "reports/summary_report.html"
    #message:
    #params:
    #scripts:
        #""

# ---------------------------------------------------
# ---------------------------------------------------
# ---------------------------------------------------

#rule create_sample_sumamry:
    #input:
        #mlst = 
        #oh = 
        #checkm = "checkm/{sample}/results_checkM_genome.tsv"
        #mpa = 
        #virulence = 
    #output:
        #summary = "summary/summary_{sample}.tsv"
        


