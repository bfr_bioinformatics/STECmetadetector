#!/usr/bin/env Rscript

# parse minimap

library(dplyr)

# definitions

if(exists("snakemake")){
  
  paf.file <- snakemake@input[["mapping_out"]] 
  sample <- snakemake@params[["sample"]] 
  min_gene_coverage <- snakemake@params[["min_gene_coverage"]] 
  min_gene_depth <- snakemake@params[["min_gene_depth"]] 
  outfile <- snakemake@output[["summary"]] 
  
} else{
  warning("Manual data input mode")
  
  paf.file <- "/cephfs/abteilung4/Projects_NGS/Metadetect/analysis/pipeline_example/mapping_characterization/Lait_37-A_vs_Ogroup_stx_eae.paf"
  #paf.file <- "/cephfs/abteilung4/Projects_NGS/Metadetect/analysis/pipeline_example/mapping_characterization/LD6_37+A_vs_Ogroup_stx_eae.paf"
  
  sample <- "Lait_37-A"
  
  #
  #do_eae <- T
  #do_H <- T
  #do_O <- T
  #do_stx <- T
  min_gene_coverage <- 0.8
  min_gene_depth <- 1
  
  outfile <- sub(".paf$","_parsed.tsv",paf.file)
} 
# read_data ----------------
paf <- read.delim(paf.file, stringsAsFactors = F, header = F) %>% as_tibble()

colnames(paf)[1:12] = c("queryID","queryLen","queryStart","queryEnd","strand","refID","refLen","refStart","refEnd","numResidueMatches","lenAln","mapQ")

# O group
Ogroup_summary <- paf %>% filter(grepl("wz[a-z]_",refID)) %>% 
  mutate(gene_coverage = lenAln/refLen) %>%
  filter(gene_coverage >= min_gene_coverage) %>%
  group_by(queryID) %>%
  filter(numResidueMatches == max(numResidueMatches)) %>% 
  #select(queryID,refID) %>%
  tidyr::separate(refID,c("wgene","allele","accession","Ogroup"),sep = "_", remove = F) %>%
  #filter(queryID == "00fa0e40-a006-471d-b141-15cbccb4d70c") %>%
  ungroup() %>%
  count(Ogroup) %>%
  arrange(desc(n))

#paste(unlist(t(Ogroup_summary)), collapse = ",")
#Ogroup_summary[1,] 

count_Ogroups <- nrow(Ogroup_summary)
detected_Ogroups <- paste(Ogroup_summary$Ogroup, collapse = "/")

Ogroup_major = ifelse(nrow(Ogroup_summary) >= 1,unlist(Ogroup_summary[1,1]),NA)
Ogroup_coveragedepth_major = ifelse(nrow(Ogroup_summary) >= 1,unlist(Ogroup_summary[1,2]),NA)

Ogroup_minor = ifelse(nrow(Ogroup_summary) >= 2,unlist(Ogroup_summary[2,1]),NA)
Ogroup_coveragedepth_minor = ifelse(nrow(Ogroup_summary) >= 2,unlist(Ogroup_summary[2,2]),NA)

#Ogroup_3 = ifelse(nrow(Ogroup_summary) >= 3,unlist(Ogroup_summary[3,1]),NA)
#Ogroup_coveragedepth_3 = ifelse(nrow(Ogroup_summary) >= 3,unlist(Ogroup_summary[3,2]),NA)

Ogroup_other = paste(Ogroup_summary$Ogroup[-c(1:2)], collapse = "/")
Ogroup_coveragedepth_other = paste(Ogroup_summary$n[-c(1:2)], collapse = "/")

# O and H on same read???

# eae
eae <- paf %>% filter(grepl("eae",refID)) %>% 
  mutate(gene_coverage = lenAln/refLen) %>%
  filter(gene_coverage >= min_gene_coverage) %>%
  group_by(queryID) %>%
  filter(numResidueMatches == max(numResidueMatches)) #%>% 
  #nrow()

count_eae = nrow(eae)
has_eae <- ifelse(nrow(eae) >= min_gene_depth ,T,F)

# stx
stx <- paf %>% filter(grepl("stx",refID)) %>%
  mutate(gene_coverage = lenAln/refLen) %>%
  filter(gene_coverage >= min_gene_coverage) %>%
  tidyr::separate(refID,c("stx","allele","accession"),sep = ":", remove = F, extra = "drop") %>%
  group_by(queryID, stx) %>%
  filter(numResidueMatches == max(numResidueMatches)) %>% 
  filter(row_number()==1) #%>%


count_stx <- stx %>% ungroup() %>%
  count(stx) %>% 
  arrange(desc(n))

#count_stx %>% filter(n >= min_gene_depth)

count_stx_types <- nrow(count_stx)
detected_stx_types <- paste(count_stx$stx, collapse = "/")

type_stx_major = ifelse(nrow(count_stx) >= 1,unlist(count_stx[1,1]),NA)
count_stx_coveragedepth_major = ifelse(nrow(count_stx) >= 1,unlist(count_stx[1,2]),NA)

type_stx_minor = ifelse(nrow(count_stx) >= 2,unlist(count_stx[2,1]),NA)
count_stx_coveragedepth_minor = ifelse(nrow(count_stx) >= 2,unlist(count_stx[2,2]),NA)

#stx %>% ungroup() %>%
#  count(stx,allele)

# stx and eae on same read

intersection_stx_eae <- intersect(stx$queryID, eae$queryID)
count_stx_eae_onsameread <- length(intersection_stx_eae)



# compile information

mapping_summary <- data.frame(
  count_Ogroups,
  detected_Ogroups,
  Ogroup_major,
  Ogroup_coveragedepth_major,
  Ogroup_minor,
  Ogroup_coveragedepth_minor,
  Ogroup_other,
  Ogroup_coveragedepth_other,
  #eae
  has_eae,
  count_eae,
  #stx
  count_stx_types,
  detected_stx_types,
  type_stx_major,
  count_stx_coveragedepth_major,
  type_stx_minor,
  count_stx_coveragedepth_minor,
  # combo
  count_stx_eae_onsameread
)


mapping_summary <- mapping_summary %>% mutate(sample = sample) %>% relocate(sample)

# write out
write.table(mapping_summary,outfile,sep="\t", row.names = F, quote = F)

# finish
message("Done parsing maooing results for sample ", sample)
