#!/usr/bin/env Rscript


# GOAL: parse abricate Ecoli vf results

library(dplyr)

# input

if(exists("snakemake")){
  
  
  list_abricate_results <- snakemake@input 
  #outfile <- snakemake@output[["ecoh_stats"]]
  outfile <- snakemake@output[[1]] 
  
} else{
  warning("Debug mode")
  
  outfile <- "/cephfs/abteilung4/Projects_NGS/Metadetect/analysis/pipeline_example/strainberry/Lait_37-A/summary/ecolivf_stats.tsv"
  
  # 
  abricate_results.file <- "/cephfs/abteilung4/Projects_NGS/Metadetect/analysis/pipeline_example/strainberry/LD6_37+A/dephased_contigs/abricate/ecolivf_unphased.tsv"
  list_abricate_results <- c(abricate_results.file,abricate_results.file,abricate_results.file)

  # example of empty file
#  abricate_results.file <- "/cephfs/abteilung4/Projects_NGS/Metadetect/analysis/pipeline_example/strainberry/Lait_37-A/dephased_contigs/abricate/ecolivf_unphased.tsv"
#  list_abricate_results <- c(list_abricate_results,abricate_results.file)
  
    
} 


# functions



define_pathotype <- function(abricate_results.file){

  abricate_results <- read.delim(abricate_results.file, stringsAsFactors = F, check.names = F)
  
  if (nrow(abricate_results) == 0){
    warning("Empty abricate result file")
    return(NULL)
  } 
  
  
  phase <- sub(".fasta$","",abricate_results$`#FILE`[1] )
  
  # define genes to search
  gene_list_fuzzy <- "stx"
  gene_list_exact <- c("eae")
  
  results_filtered_1 <- abricate_results %>% filter(grepl(gene_list_fuzzy,GENE))
  results_filtered_2 <- abricate_results %>% filter(GENE %in% gene_list_exact)
  
  stx <- results_filtered_1 %>% select(GENE) %>% unlist() %>% sort() %>% paste(.,collapse = ";")
  eae <- results_filtered_2 %>% select(GENE) %>% unlist() %>% sort() %>% paste(.,collapse = ";")
  
  
  # define pathotype
  pathotype <- ifelse(grepl("stx",stx) & grepl("eae",eae), "EHEC", 
                      ifelse(grepl("stx",stx),"STEC","NON-STEC"))
  
  #return
  data.frame(
    phase = phase,
    stx = stx,
    eae = eae,
    pathotype = pathotype
  )
}
  
# execution

# compute
pathotype_summary <- do.call(rbind,lapply(list_abricate_results,define_pathotype))

# export
write.table(pathotype_summary, outfile, sep = "\t", row.names = F, quote = F)
