General summary:         
Mean read length:              9,547.7
Mean read quality:                14.2
Median read length:            4,849.0
Median read quality:              14.5
Number of reads:               1,000.0
Read length N50:              19,397.0
STDEV read length:            12,112.0
Total bases:               9,547,703.0
Number, percentage and megabases of reads above quality cutoffs
>Q5:	1000 (100.0%) 9.5Mb
>Q7:	1000 (100.0%) 9.5Mb
>Q10:	975 (97.5%) 9.4Mb
>Q12:	852 (85.2%) 8.1Mb
>Q15:	409 (40.9%) 4.0Mb
Top 5 highest mean basecall quality scores and their read lengths
1:	19.3 (1472)
2:	19.0 (2942)
3:	18.7 (3210)
4:	18.5 (2660)
5:	18.5 (1318)
Top 5 longest reads and their mean basecall quality score
1:	92229 (16.5)
2:	82258 (14.0)
3:	79873 (11.9)
4:	78687 (15.1)
5:	77491 (11.4)
