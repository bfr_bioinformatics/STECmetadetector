# include this if used standalone
import os
import pandas as pd
import shutil
shell.executable("bash")


# rules for dynamically parsing and post-processing strainberry results


#workdir: "/cephfs/abteilung4/Projects_NGS/Metadetect/analysis/pipeline_example"
#SAMPLE = "LD6_37+A"
#SAMPLE = "Lait_37-A"
#SAMPLE_list = ["LD6_37+A","Lait_37-A" ]

# Set snakemake main workdir variable
workdir: config["workdir"]

# samples
samples = pd.read_table(config["samples"], index_col="sample")
samples.index = samples.index.astype('str', copy=False) # in case samples are integers, need to convert them to str



# -----------------------

rule all_checkpoint_strainberry:
    input:
        expand("strainberry/{sample}/summary/ecoh_stats.tsv", sample = samples.index),
        expand("strainberry/{sample}/summary/ecolivf_stats.tsv", sample = samples.index),
        expand("strainberry/{sample}/summary/results_phase.tsv", sample = samples.index),
        "summary/strainberry_summary.tsv"
        #expand("dephased_contigs/abricate/ecoh_summary.tsv", sample = SAMPLE_list)
        #expand("strainberry/{sample}/dephased_contigs/abricate/ecoh_summary.tsv", sample = SAMPLE)
        #output: "strainberry/{sample}/summary/ecoh_stats.tsv"


# rules --------------------------


checkpoint dephasing:
    input:
        strainberry_completed = "strainberry/{sample}/.completed"
    output:
        strainberry_dephased_contigs_dir = directory("strainberry/{sample}/dephased_contigs"),
        strainberry_stats = "strainberry/{sample}/summary/dephasing_stats.tsv",
        #clustering_dir = directory("merged_db/clustering"),
        #orphansamples_file = "merged_db/orphan_samples.tsv"
    message: "Dephasing strainberry results of sample {wildcards.sample}"
    #log: "logs/strainberry_dephasing.log"
    params:
        strainberry_dir = "strainberry/{sample}/",
        minimum_phase_length = 200000,
        optimal_phase_length = 4000000,
        skip_small_phases = True
    conda: "envs/rmarkdown.yaml"
    script:
        "../scripts/parse_strainberry.R"
        #"../scripts/strainberry_dephasing.R"

# simpler
    #shell:
        #"""
        #grep '>' {params.strainberry_dir}/strainberry_n2/20-assembly/assembly.contigs.fa | tr ' ' '\t' | tr -d '>' > {params.strainberry_dir}/summary/contig_stats.tsv
        #cut -f 2 {params.strainberry_dir}/summary/contig_stats.tsv | sort | uniq -c | sed -E 's/^[ ]+//' | awk -F ' ' '{{print $2"\t"$1}}' > {output.strainberry_stats}
        #mkdir -p {output.strainberry_dephased_contigs_dir}        
        #cat {params.strainberry_dir}/summary/contig_stats.tsv | while read phase count; do
            #echo "$phase;$count" > {output.strainberry_dephased_contigs_dir}/$phase.fasta
        #done
        #"""

#        {params.strainberry_dir}/strainberry_n2/20-assembly/assembly.contigs.fa

# ------

#strainberry_dummyfile="/cephfs/abteilung4/Projects_NGS/Metadetect/analysis/pipeline_example/strainberry/LD6_37+A/.completed"
#strainberry_dir=`dirname $strainberry_dummyfile`


#if [[ -f $strainberry_dir/strainberry_n3/20-assembly/assembly.contigs.fa ]]; then
    #echo "3 phases"
    #contigs="$strainberry_dir/strainberry_n3/20-assembly/assembly.contigs.fa"
#elif [[ -f $strainberry_dir/strainberry_n2/20-assembly/assembly.contigs.fa ]]; then
    #contigs="$strainberry_dir/strainberry_n2/20-assembly/assembly.contigs.fa"
    #echo "2 phases"
#else
    #echo "monophasic"
    #contigs="NA"
#fi



# ------


rule type_dephased_contigs:
    input:
        dephased_contigs = "strainberry/{sample}/dephased_contigs/{phase}.fasta",
    output:
        report = "strainberry/{sample}/dephased_contigs/abricate/ecoh_{phase}.tsv",
    log: "logs/type_dephased_contigs_{sample}/{phase}.log"
    message: "typing dephased contigs"
    params:
        refdb = config["parameters"]["OH"]["path"],
        dbname = config["parameters"]["OH"]["name"],
        minid = config["parameters"]["OH"]["minid"],
        mincov = config["parameters"]["OH"]["mincov"]
    conda: "envs/abricate.env"
    shell:
        """
        abricate --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.dephased_contigs} > {output.report} 2> >(tee {log} >&2)
        """
        #"cat {input.dephased_contigs} > {output.report}" # dummy
    
    
# stx typing
rule run_abricate_ecoli_vf_phase:
    input:
        dephased_contigs = "strainberry/{sample}/dephased_contigs/{phase}.fasta",
    output:
        report = "strainberry/{sample}/dephased_contigs/abricate/ecolivf_{phase}.tsv",
    #conda: "envs/abricate.yaml"
    #log:
        #"logs/abricate_ecoli_vf_{sample}.log"
    #log: "logs/abricate_ecolivf_dephased_contigs_{sample}/{phase}.log"
    #message: "abricate ecolivf dephased contigs"
    threads:
        config["parameters"]["threads"]
    params:
        refdb = config["parameters"]["ecovf"]["path"],
        dbname = config["parameters"]["ecovf"]["name"],
        minid = config["parameters"]["ecovf"]["minid"],
        mincov = config["parameters"]["ecovf"]["minid"]
    shell: 
        "abricate --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.dephased_contigs} > {output.report} 2> >(tee {log} >&2)"





# mlst

# checkm

def aggregate_phasedcontigs_ecoh(wildcards):
    checkpoint_output = checkpoints.dephasing.get(**wildcards).output[0]
    return expand("strainberry/{{sample}}/dephased_contigs/abricate/ecoh_{phase}.tsv",
        phase=glob_wildcards(os.path.join(checkpoint_output, "{phase}.fasta")).phase)


def aggregate_phasedcontigs_ecolivf(wildcards):
    checkpoint_output = checkpoints.dephasing.get(**wildcards).output[0]
    return expand("strainberry/{{sample}}/dephased_contigs/abricate/ecolivf_{phase}.tsv",
        phase=glob_wildcards(os.path.join(checkpoint_output, "{phase}.fasta")).phase)



rule summarize_ecoh_phasedcontigs:
    input: aggregate_phasedcontigs_ecoh
    output: 
        ecoh_stats = "strainberry/{sample}/summary/ecoh_stats.tsv"
    message: "Aggregating all phases ecoh results"
    message: "summarize_ecoh_phasedcontigs"
    conda: "envs/rmarkdown.yaml"
    script:
        "../scripts/parse_oh_phase.R"
    
rule summarize_ecolivf_phasedcontigs:
    input: aggregate_phasedcontigs_ecolivf
    output: 
        ecolivf_stats = "strainberry/{sample}/summary/ecolivf_stats.tsv"
    message: "Aggregating all phases ecolivf results"
    conda: "envs/rmarkdown.yaml"
    script:
        "../scripts/parse_ecolivf_phase.R"


rule merge_phase_information:
    input:
        ecolivf_stats = "strainberry/{sample}/summary/ecolivf_stats.tsv",
        ecoh_stats = "strainberry/{sample}/summary/ecoh_stats.tsv",
        strainberry_stats = "strainberry/{sample}/summary/dephasing_stats.tsv",
    output:
        results = "strainberry/{sample}/summary/results_phase.tsv",
        summary = "strainberry/{sample}/summary/summary_phase.tsv"
    message: "merge_phase_information for sample {wildcards.sample}"
    conda: "envs/rmarkdown.yaml"
    script:
        "../scripts/merge_phase_information.R"



# summarize all

rule aggregate_phase_information_allsamples:
    input: 
        stats = expand("strainberry/{sample}/summary/summary_phase.tsv", sample = samples.index)
    output: 
        summary_tsv = "summary/strainberry_summary.tsv"
    message: "aggregate_phase_information_allsamples"
    run:
        with open(output.summary_tsv, 'wb') as outfile:
            for i, fname in enumerate(input.stats):
                with open(fname, 'rb') as infile:
                    if i != 0:
                        infile.readline()  # Throw away header on all but first file
                    shutil.copyfileobj(infile, outfile)      # Block copy rest of file from input to output without parsing
