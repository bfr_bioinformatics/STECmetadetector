# test data
* small dummy set

2 isolate data subsampled to 10000 reads

$ bioawk -c fastx '{if(NR <= 10000) print "@"$name" "$comment"\n"$seq"\n+\n"$qual}' 6423-O26-barcode18.fastq.gz | gzip  > dummy/6423-O26-barcode18.fastq.gz
$ bioawk -c fastx '{if(NR <= 10000) print "@"$name" "$comment"\n"$seq"\n+\n"$qual}' 4747-O26-barcode13.fastq.gz | gzip  > dummy/4747-O26-barcode13.fastq.gz 



* realitic test set

# conda environment

mamba 
conda install snakemake-minimal porechop nanofilt kraken2 krakentools flye blast nanoplot abricate 

#for strainberry ? it also included minimap2

# database
kraken2 minikraken
blast stx_eae_Ogroup


# design sample sheet

sample\treads


