#!/usr/bin/env python3

import argparse
import subprocess
import os
import sys
import pandas as pd
from yaml import dump

version = "0.1.2"


def get_gitversion(fixed_version: str):
    cwd = os.getcwd()
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    try:
        git_hash = subprocess.check_output(["git", "describe", "--always"]).decode('ascii').strip()
    except FileNotFoundError:
        git_hash = fixed_version
    os.chdir(cwd)  # NOTE: logdir argument removed
    return git_hash


def create_config(configfile, args):
    print("You are using metadetect version " + git_version)

    # checks
    if not os.path.exists(args.sample_list):
        print("error: path to sample list " + args.sample_list + " does not exist")
        sys.exit(1)

    if not os.path.exists(args.kraken2db):
        print("error: path to kraken2 database " + args.kraken2db + " does not exist")
        sys.exit(1)
    
    print("nano-hq: ")
    print(args.nano_hq)
    read_type = '--nano-hq' if args.nano_hq else '--nano-raw'
    print(read_type)

    config_dict = {
        'Created by Metadetect': version,
        'version'           : git_version,
        'workdir'           : args.working_directory,
        'samples'           : args.sample_list,
        'parameters'            : {
            'threads'    : args.threads_sample,
#            'remove_temp': args.remove_temp,
            'kraken2db': args.kraken2db,
            'taxid': 562, 
#
            'filtering'      : {
                'minlength': args.minlength,
                'minquality': args.minquality,
                'extra': ""
                },
            'flye': {
                'read_type': read_type,
                'iterations': args.flye_iterations,
                'extra': '--meta'
                },
            'OH': {
                'path': args.abricate_db,
                'name': "ecoh",
                'minid': args.minid,
                'mincov': args.mincov
                },
            'ecovf': {
                'path': args.abricate_db,
                'name': "ecoli_vf",
                'minid': args.minid,
                'mincov': args.mincov
                },
            'blastn': {
                    'db': args.blast_db
                    },
            'kraken2'    : {'db_kraken'         : args.kraken2db,
                            'taxid'       : 562 # args.taxid
                            }
                }
            }




    with open(configfile, 'w') as outfile:
        dump(config_dict, outfile, default_flow_style=False, sort_keys=False)




def run_snakemake(configfile, args, sample_list):


# more fine control


    if args.forceall:
        force = "--forceall"
    elif not args.force:
        force = ""
    else:
        force = "-f {args.force}"

    if args.unlock:
        unlock = "--unlock"
    else:
        unlock = ""

    if args.dryrun:
        dryrun = "-n"
    else:
        dryrun = ""

    if args.remove_temp:
        remove_temp = ""
    else:
        remove_temp = "--notemp"

    if args.conda_frontend:
        frontend = "conda"
    else:
        frontend = "mamba"

    if args.use_conda:
        call = "snakemake --conda-prefix {CondaPrefix} --keep-going --configfile {configfile} --config samples={sample_list} --snakefile {snakefile} {remove_temp} {force} {dryrun} {unlock} --cores {threads} --use-conda --conda-frontend {frontend}".format(
            CondaPrefix=args.condaprefix, snakefile=args.snakefile, configfile=configfile, sample_list=sample_list, remove_temp=remove_temp, force=force, dryrun=dryrun, unlock=unlock, threads=args.threads, frontend=frontend)
    else:
        call = "snakemake --keep-going --configfile {configfile} --config samples={sample_list} --snakefile {snakefile} {remove_temp} {force} {dryrun} {unlock} --cores {threads}".format(
            CondaPrefix=args.condaprefix, snakefile=args.snakefile, configfile=configfile, sample_list=sample_list, remove_temp=remove_temp, force=force, dryrun=dryrun, unlock=unlock, threads=args.threads)

    print(call)
    subprocess.call(call, shell=True)




def main():



    print("This is STECmetadetector, version " + version) 
    
    
    #find out path of abricate
    conda_path = os.environ['CONDA_PREFIX']
    db_dir = os.path.join(conda_path,"db")

    # find out repo path
    # repo_path = os.path.dirname(os.path.realpath(__file__)) # needed?

    parser = argparse.ArgumentParser()

    # path arguments
    parser.add_argument('-l', '--sample_list', help='List of samples to assemble, format as defined by ...',
                        default="/", type=os.path.abspath, required=False)
    parser.add_argument('-d', '--working_directory', help='Working directory',
                        default="/", type=os.path.abspath, required=False)
    parser.add_argument('-s', '--snakefile', help='Path to Snakefile of bakcharak pipeline, default is path to Snakefile in same directory', default = os.path.join(os.path.dirname(os.path.realpath(__file__)),"Snakefile"),required=False)



    # TODO add database paths and parameters


    # cpu arguments
    parser.add_argument('-t', '--threads', help='Number of Threads to use. Ideally multiple of 10; default: 10',
                        default=10, type=int, required=False)
    parser.add_argument('-p', '--threads_sample', help='Number of Threads to use per sample; default: 5',
                        default=5, type=int, required=False)
    parser.add_argument('-c', '--condaprefix', help='Path of default conda environment, enables recycling built environments; default: "<milonga>/conda_env"',
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "conda_env"), required=False)
    parser.add_argument('-n', '--dryrun', help='Snakemake dryrun. Only calculate graph without executing anything',
                        default=False, action='store_true', required=False)
    parser.add_argument('--forceall', help='Snakemake force. Force recalculation of all steps',
                        default=False, action='store_true', required=False)
    parser.add_argument('-f', '--force', help='Snakemake force. Force recalculation of output (rule or file) speciefied here',
                        default=False, type=str, required=False)
    parser.add_argument('--unlock', help='Unlock a snakemake execution folder if it had been interrupted',
                        default=False, action='store_true', required=False)
    parser.add_argument('--remove_temp', help = 'Remove large temporary files. May lead to slower re-runs but saves disk space.',
                        default = False, action = 'store_true', required = False)
    parser.add_argument('--use_conda', help='Utilize the Snakemake "--useconda" option, i.e. Smk rules require execution with a specific conda env',
                        default=False, action='store_true', required=False)
    parser.add_argument('--conda_frontend', help='Do not use mamba but conda as frontend to create individual conda environments',
                        default=False, action='store_true', required=False)
    parser.add_argument('-V', '--version', help='Print program version',
                        default=False, action='store_true', required=False)


    # databases
    #args.abricate_db
    # abricate parameters
    parser.add_argument('--minid', help='Minimum identity in abricate, default=80', default=80, required=False)
    parser.add_argument('--mincov', help='Minimum coverage in abricate, default=50', default=50, required=False)
    parser.add_argument('--abricate_db', help='Path to abricate and prokka databases, default is database of conda environment', default=db_dir,required=False, type=os.path.abspath)


    # kraken2
    parser.add_argument('--kraken2db', help='Path to kraken2 database; default: ' + os.path.join(os.path.dirname(os.path.realpath(__file__)), "databases", "kraken2"),
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "databases", "kraken2"), type=os.path.abspath, required=False)  # Note: minikraken2.tar.gz extracts to "../kraken/" not kraken2
    
    # blast
    parser.add_argument('--blast_db', help='Path to blast database of abricate and prokka databases, default is ' + os.path.join(os.path.dirname(os.path.realpath(__file__)), "databases", "db_stx_eae_O-group","Ogroup_stx_eae.fasta"), default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "databases", "db_stx_eae_O-group","Ogroup_stx_eae.fasta"),required=False, type=os.path.abspath)
    
    # flye
    
    parser.add_argument('--nano-hq' , help='Run flye with nano-hq mode (ONT high-quality reads: Guppy5+ or Q20  ) instead if --nano-raw (default)', default=False, action='store_true', required=False)
    #parser.add_argument('--flye_read_type', help='flye read type: choose from \'--pacbio-raw\'   \'--pacbio-corr\'   \'--pacbio-hifi\'   \'--nano-raw\'   \'--nano-corr\'   \'--subassemblies\', default="--nano-raw"', default="--nano-raw", required=False)
    parser.add_argument('--flye_iterations', help='Number of self-polising iterations in flye, default=3', default=3, required=False)
    #parser.add_argument('--flye_extra', help='Extra parameters for flye, enclose in \', default=""', default="", required=False)
    
    # other
    parser.add_argument('--minlength', help='Minimum read length for NanoFilt, default=1000', default=1000, required=False)
    parser.add_argument('--minquality', help='Minimum Phred-score Quality for NanoFilt, default=7', default=7, required=False)
    
    # finish parsing

    args = parser.parse_args()

    # git version
    global git_version
    git_version = get_gitversion(fixed_version=version)

    if args.version:
        print("metadetect version ", version)
        if not version == git_version:
            print("Commit version ", git_version)
        return

    # checks
    if args.sample_list == "/":
        print("error: the following argument is required: -l/--sample_list")
        sys.exit(1)

    if args.working_directory == "/":
        print("error: the following argument is required: -d/--working_directory")
        sys.exit(1)

    if not os.path.exists(args.working_directory):
        os.makedirs(args.working_directory)

    configfile = os.path.join(args.working_directory, "config.yaml")
    create_config(configfile, args)

    run_snakemake(configfile, args, sample_list=args.sample_list)

if __name__ == '__main__':
    main()
